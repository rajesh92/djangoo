from presales_bot.dataprep.psb_dataprep_map import *
import numpy as np
from chatbot.settings import BASE_DIR

data_prep_conf = {
    "GLOBAL_OPT":
        {

        },
    "BOT_MATCH":
        {
            "PRESALES_BOT_PROD":
                {
                    "OUTPUT_FILE_PATH": BASE_DIR + "/chatbot/models/presales_bot/dataprep/presales_sb_solr_df.csv",

                    "config_seq": ['READ_DATA', 'PROCESS_SOURCE', 'DATA_POSTPROCESS'],

                    "READ_DATA": {
                        'func': 'eapl_read_raw_data_new',
                        'rd_src_data': [

                            {'data_type': 'fileio',
                             'file_path': BASE_DIR + "/chatbot/models/presales_bot/dataprep/data_files/demostore_180520.csv",
                             'file_type': 'csv',
                             'sheet_name': None,
                             'output_df': 'demostore_org_df',
                             'encoding': None,
#                             'nrows': 1
                             },
                            {
                                'data_type': 'fileio',
                                'file_path': BASE_DIR + "/chatbot/models/presales_bot/dataprep/data_files/sap_digital_library_dam_180520.csv",
                                'file_type': 'csv',
                                'sheet_name': 'Videos',
                                'output_df': 'dig_lib_org_df',
                                'encoding': None,
#                                'nrows': 1
                            },
                            {
                                'data_type': 'fileio',
                                'file_path': BASE_DIR + "/chatbot/models/presales_bot/dataprep/data_files/sf_sml_180520.csv",
                                'file_type': 'csv',
                                'output_df': 'sf_lms_org_df',
                                'encoding': None,
 #                               'nrows': 1
                            },
                            {
                                'data_type': 'fileio',
                                'file_path': BASE_DIR + '/chatbot/models/presales_bot/dataprep/data_files/psb_tags_map.csv',
                                'file_type': 'csv',
                                'sheet_name': None,
                                'output_df': 'psb_tags_df',
                                'encoding': None
                            },
                        ]
                    },

                    "PROCESS_SOURCE": {

                        'func': 'eapl_process_source_new',

                        'src_proc_cfg':
                            [
                                {
                                    "source": "demo_store",
                                    "process_data": [
                                        {
                                            "input_type": "df",
                                            "output_type": "df",
                                            "input": ['demostore_org_df'],
                                            "output": 'demostore_df',
                                            "ops": [
                                                ['map_typecast_sc', (demostore_col_map, None, None, None)],
                                                ['strip', ('asset_id', 'lstrip', '0', 'asset_id')],
                                                ['assign_cols', ('url',
                                                                 'https://sapdemostore.com/sap/bc/ui5_ui5/sap/yunifiedstore/index.html#/scenario/')],
                                                ['append_msgs', (['url', 'asset_id'], '%s%s', 'url')],
                                                ['assign_cols', ('source', 'Demo Store')],
                                                ['replace_strip', ('content_type', 'None', 'Learning Script, Desktop Demo, Selling Script', 'content_type')],
                                                ['np_where_eval_vv', ("top_demo == 'Yes'", 'Top Demo', '', 'top_demo')],


                                            ]
                                        }
                                    ],

                                },

                                {
                                    "source": "digital library",
                                    "process_data": [
                                        {
                                            "input_type": "df",
                                            "output_type": "df",
                                            "input": ['dig_lib_org_df'],
                                            "output": 'dig_lib_df',
                                            "ops": [
                                                ['map_typecast_sc', (digital_library_col_map, None, None, None)],
                                                ['assign_cols', ('source', 'Digital Library')],
                                            ],
                                        }
                                    ],

                                },
                                {
                                    "source": "sf",
                                    "process_data": [
                                        {
                                            "input_type": "df",
                                            "output_type": "df",
                                            "input": ['sf_lms_org_df'],
                                            "output": 'sf_lms_df',
                                            "ops": [
                                                ['map_typecast_sc', (sf_col_map, None, None, None)],
                                                ['assign_cols', ('source', 'Success Factors')],
                                                ['re_ext_pat', ('asset_id', "revisionDate", "\d{1,2}/\d{1,2}/\d{4} (?:2[0-3]|[01]?[0-9]):[0-5]?[0-9]", 0)],
                                                ['re_ext_pat', ('asset_id', "componentID", " (.*?) \(", 1)],
                                                ['split_col', ('asset_id', "componentTypeID", " ", 0)],
                                                ['replace_strip', ('componentTypeID', '&', '%2526', 'componentTypeID')],
                                                ['replace_strip', ('componentTypeID', '#', '%2523', 'componentTypeID')],
                                                ['replace_strip', ('componentTypeID', '@', '%2540', 'componentTypeID')],
                                                ['replace_strip', ('componentTypeID', '/', '%252F', 'componentTypeID')],
                                                ['epoch_datetime', ('revisionDate', "epoch_revisionDate", "%m/%d/%Y %H:%M", "UTC")],
                                                ['eval_cols', ("epoch_revisionDate_1000=epoch_revisionDate*1000")],
                                                ['append_msgs', (['componentID', 'componentTypeID', 'epoch_revisionDate_1000'],
                                                                  "https://sap.plateau.com/learning/user/deeplink_redirect.jsp?linkId=ITEM_DETAILS&componentID=%s&componentTypeID=%s&revisionDate=%d&fromSF=Y&company=SAP",
                                                                 'url')],
                                                ['assign_cols', ('url_prefix', 'https://performancemanager5.successfactors.eu/sf/learning?destUrl=')],
                                                ["url_encode", ('url', 'url', 'UTF-8', '')],
                                                ['replace_strip', ('url', '-', '%2D', 'url')],
                                                ['replace_strip', ('url', '.', '%2E', 'url')],
                                                ['replace_strip', ('url', '_', '%5F', 'url')],
                                                ['replace_strip', ('url', '+', '%2B', 'url')],
                                                ['replace_strip', ('url', '%26company%3DSAP', '&company=SAP', 'url')],
                                                ['append_msgs', (['url_prefix', 'url'],
                                                                  "%s%s",
                                                                  'url')],
                                                ['str_contains', ('componentTypeID', 'COURSE', 'course_flg')],
                                                ['filter', ("course_flg==True", True)],
                                                ['drop_cols', (['componentID', 'componentTypeID', 'epoch_revisionDate_1000', 'revisionDate', 'epoch_revisionDate', 'url_prefix', 'course_flg'])],





                                            ],
                                        }
                                    ],

                                },
                            ]
                    },

                    'DATA_POSTPROCESS':
                        {
                            'func': 'eapl_dataprep_postprocess',
                            "process_data":
                                {
                                    'tags_df': 'psb_tags_df',
                                    'concat_df_list': ['demostore_df', 'dig_lib_df', 'sf_lms_df'],
                                    'str_proc_cols': ['title', 'desc', 'attachment', 'notes'],
                                    'dedup_cols': ['title', 'url'],
                                    'output_df': 'psb_solr_df',
                                    'unq_id_prefix': 'psb_'
                                },
                        },

                }
        },

        }
